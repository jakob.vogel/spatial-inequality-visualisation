# Dieses Skript berechnet die Spearman-Rankkorrelation zwischen verschiedenen Indikatoren, die Flacke et al. benutzen, und will die Tabellen 4 bis 6 aus ihrem Paper rekonstruieren
import pandas as pd
from scipy.stats import spearmanr

# Lade Daten. Die CSV-Datei habe ich manuell erstellt, indem ich die Daten aus der PDF-Tabelle im Anhang des Papers herauskopiert habe
data_path = 'Data/Flacke_Table_S1.csv'
path_out = 'Data/An/'
df = pd.read_csv(data_path)

# Rekonstruktion Flacke et al. Tabelle 4:
# Berechne Spearman-Rangkorrelation mit entsprechender Pandas-Methode
# Definiere relevante Spalten
columns_of_interest = ['Migration (%)', 'Unemployment (%)', 'Welfare (%)', 'Socioeconomic Disadvantage (%)']

# Benutze Pandas' Spearman-Methode 
spearman_corr = df[columns_of_interest].corr(method='spearman')

# Berechne P-Werte
p_values = df[columns_of_interest].apply(lambda x: df[columns_of_interest].apply(lambda y: spearmanr(x, y)[1]))

# Runde Werte auf 3 Nachkommastellen
spearman_corr_rounded = spearman_corr.round(3)
p_values_rounded = p_values.round(3)

# Speichere Tabellen und gib sie auf der Konsole aus
spearman_corr_rounded.to_csv(f'{path_out}flacke_spearman_rank_coefficients_4.csv')
p_values_rounded.to_csv(f'{path_out}flacke_p_values_4.csv')

print('Spearman-Rangkorrelation:')
print(spearman_corr_rounded)
print('--------------------------------')

print('\nP-Werte:')
print(p_values_rounded)
print('--------------------------------')

# Gib Wahrheitsmatrix aus, die abbildet, welche der P-Werte unter 0.05 liegen (also derselbe Test, den Flacke et al. durchführen)
is_below_005 = p_values_rounded < 0.05
print('\nP-Werte unter 0.05:')
print(is_below_005)

"""
Berrechnete Werte weichen ab der 2. bzw. 3. Nachkommastelle von denen ab, die Flacke et al. in ihrem Paper angeben. Das kann verschiedene Gründe haben. Der wahrscheinlichste ist, dass Flacke et al. bestimmte Preprocessing-Maßnahmen vornehmen, die sie nicht im Paper nennen, bzw. die veröffentlichten Daten erst nach ihrer Analyse gerundet haben. Da die Abweichungen so gering sind, können sie vernachlässigt werden. Die starke Korrelation zwischen allen Indikatoren und insbesondere zwischen Socioeconomic Distance und dem Rest wird so oder so deutlich.
"""

# Rekonstruktion Flacke et al. Tabelle 5 und 6:
# Dieselbe Prozedur, nur mit anderen Spalten
columns_of_interest = ['Socioeconomic Disadvantage (%)', 'Green (%)', 'Noise (%)', 'NO_2 (%)', 'PM_10 (%)']
spearman_corr = df[columns_of_interest].corr(method='spearman')
p_values = df[columns_of_interest].apply(lambda x: df[columns_of_interest].apply(lambda y: spearmanr(x, y)[1]))

spearman_corr_rounded = spearman_corr.round(3)
# Runde p-Werte hier ausnahmsweise auf 5 Stellen, damit der Signifikanz-Unterschied zwischen den Koeffizienten Socioeco D. / NO_2 und Socioeco D. / PM_10 deutlich wird (PM_10 ist etwas kleiner, daher statistisch etwas signifikanter)
p_values_rounded = p_values.round(5)

spearman_corr_rounded.to_csv(f'{path_out}flacke_spearman_rank_coefficients_5_6.csv')
p_values_rounded.to_csv(f'{path_out}flacke_p_values_5_6.csv')

print('Spearman-Rangkorrelation:')
print(spearman_corr_rounded)
print('--------------------------------')

print('\nP-Werte:')
print(p_values_rounded)
print('--------------------------------')

is_below_005 = p_values_rounded < 0.05
print('\nP-Werte unter 0.05:')
print(is_below_005)

"""
Auch hier entsprechen die Werte nicht genau denen, die Flacke et al. angeben, teilweise sogar ab der 1. Nachkommastelle. Die moderaten Korrelationen zeichnen sich aber in beiden Fällen ab, die Signifikanztests ergeben zudem dieselben Ergebnisse. Für die Tabellen, die ich in meine Masterarbeit auf Basis von Flacke et al. einbaue, benutze ich meine eignen Werte, gerundet auf 5 Nachkommastellen.
"""

# Konstruktion neuer Tabelle mit Versiegelte Flächen und nur Feinstaub (PM_10)
# Erstelle neue Spalte, die Versiegelte Flächen repräsentiert, durch Abzug der Spalte 'Green (%)' von 100%
df['Sealed (%)'] = 100 - df['Green (%)']
# Jetzt fahre fort mit Prozedur wie oben, nur mit anderen Spalten
columns_of_interest = ['Socioeconomic Disadvantage (%)', 'Sealed (%)', 'Noise (%)', 'PM_10 (%)']
spearman_corr = df[columns_of_interest].corr(method='spearman')
p_values = df[columns_of_interest].apply(lambda x: df[columns_of_interest].apply(lambda y: spearmanr(x, y)[1]))

spearman_corr_rounded = spearman_corr.round(3)
p_values_rounded = p_values.round(3)

spearman_corr_rounded.to_csv(f'{path_out}flacke_spearman_rank_coefficients_own.csv')
p_values_rounded.to_csv(f'{path_out}flacke_p_values_own.csv')

print('Spearman-Rangkorrelation:')
print(spearman_corr_rounded)
print('--------------------------------')

print('\nP-Werte:')
print(p_values_rounded)
print('--------------------------------')

is_below_005 = p_values_rounded < 0.05
print('\nP-Werte unter 0.05:')
print(is_below_005)

