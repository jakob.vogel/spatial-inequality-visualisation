# Dieses Skript erstellt zwei generische Balkendiagramme, die exemplarisch für Spatial Inequality Visualisation stehen
import matplotlib.pyplot as plt

path_out = 'Data/Vis/'
# Initiiere zufällige Daten als Listen
stadtteile = ['Stadtteil A', 'B', 'C', 'D']
indikator_sozial = [1,2,3,1]
indikator_raum = [1.5,2.6,2.7,1.7]


# Definiere Plot-Parameter für doppeltes Balkendiagramm
barwidth = 0.35

x = range(len(stadtteile))

plt.bar(x, indikator_sozial, width=barwidth, color='y', label = 'S')
plt.bar([i + barwidth for i in x], indikator_raum, width=barwidth, color='b', label = 'R')

plt.xlabel(None)
plt.ylabel(None)
plt.title(None)
plt.xticks([i + barwidth/2 for i in x], stadtteile)
plt.legend()

# Entferne y-axis-Lables und kleine Vertikalstriche
plt.gca().axes.yaxis.set_ticklabels([])
plt.gca().tick_params(axis='y', which='both', length=0)

# Speichere und zeige Bild an
plt.savefig(f'{path_out}balkendiagramm_generisch_2_indikatoren_pro_stadtteil.png', dpi=300, bbox_inches='tight')
plt.show()


# Visualisierung: Balkendiagramm Spatial Inequality pro Stadtteil, vier Indikatoren nebeneinandergestellt, generisch
# Initiiere weitere Daten als Listen
indikator_raum2 = [1.8,3,2.5,1.5]
indikator_raum3 = [0.7,1,1.3,1.8]

# So wie oben, nur mit anderer barwidth und zwei weiteren plt.bar()-Aufrufen
barwidth = 0.2
plt.bar(x, indikator_sozial, width=barwidth, color='y', label = 'S')
plt.bar([i + barwidth for i in x], indikator_raum, width=barwidth, color='b', label = 'R1')
plt.bar([i + 2*barwidth for i in x], indikator_raum2, width=barwidth, color='m', label='R2')
plt.bar([i + 3*barwidth for i in x], indikator_raum3, width=barwidth, color='c', label='R3')

plt.xlabel(None)
plt.ylabel(None)
plt.title(None)
plt.xticks([i + 1.5*barwidth for i in x], stadtteile)
plt.legend()

plt.gca().axes.yaxis.set_ticklabels([])
plt.gca().tick_params(axis='y', which='both', length=0)

plt.savefig(f'{path_out}balkendiagramm_generisch_4_indikatoren_pro_stadtteil.png', dpi=300, bbox_inches='tight')
plt.show()


