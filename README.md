# Spatial Inequality Visualisation

Dieses Repository enthält alle Datensätze und Codeausschnitte, die ich im Rahmen meiner Masterarbeit "Spatial Inequality Visualisation" im Sommersemester 2024 für meine Datenanalysen und -visualisierungen benutzt habe. 

## Author
Jakob Vogel, M.A. Digital Humanities, Universität Göttingen

## Lizenz
CC-BY-SA
Bitte beachten Sie meine Masterarbeit für weitere Informationen zur Herkunft der verwendeten Daten.
