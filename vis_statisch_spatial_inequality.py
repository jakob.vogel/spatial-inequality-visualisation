# Dieses Skript erstellt verschiedene Choropleth-Karten zur Visualisierung von Spatial Inequality in Dortmund, basierend auf Flacke et al.
import pandas as pd
import geopandas as gpd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.colors as mcolors
from adjustText import adjust_text

# Lade Daten
data_path = 'Data/Flacke_Table_S1.csv'
shapefile_path = 'Data/Dortmund/Stat_Unterbezirke/Statist_UnterbezirkePolygon.shp'
path_out = 'Data/Vis/'
ddf = pd.read_csv(data_path)
gdf = gpd.read_file(shapefile_path)

# Pre-Processing wie in anderen Skripts
"""
Nicht alle Werte für 'bezeichnun' in gdf entsprechen denen für 'Name' in ddf. Um diejenigen zu identifizieren, die nicht übereinstimmen, nutze:

gdf_unique = set(gdf['bezeichnun'].unique())
ddf_unique = set(ddf['Name'].unique())
gdf_not_in_ddf = gdf_unique - ddf_unique
ddf_not_in_gdf = ddf_unique - gdf_unique

if not gdf_not_in_ddf and not ddf_not_in_gdf:
    print("Unique values in both columns are the same. Proceed with merging.")
else:
    print("Unique values in both columns are not the same. Please verify the data.")
    print("Values in GeoDataFrame not in DataFrame:", gdf_not_in_ddf)
    print("Values in DataFrame not in GeoDataFrame:", ddf_not_in_gdf)
"""
# Sobald bekannt ist, welche Felder nicht übereinstimmen, ändere Felder in gdf entsprechend
# gdf enthält Uppercase, ändere alle Namen zu Titlecase
gdf['bezeichnun'] = gdf['bezeichnun'].apply(lambda x: x.title())

# Sonstige Änderungen, um ggf an ddf anzupassen
gdf.loc[gdf['bezeichnun'] == 'Deutsch-Luxemburger-Straße', 'bezeichnun'] = 'Luxemburger-Straße'
gdf.loc[gdf['bezeichnun'] == 'Siedlung Rotkehlchenweg', 'bezeichnun'] = 'Rotkehlchenweg'
gdf.loc[gdf['bezeichnun'] == 'Msa-Siedlung', 'bezeichnun'] = 'MSA-Siedlung'
gdf.loc[gdf['bezeichnun'] == 'Knappschaftskrankenhaus', 'bezeichnun'] = 'Knappschaftkrankenhaus'

# Merging und hinzufügen neuer Spalten wie in anderen Skripts
mdf = gdf.merge(ddf, left_on='bezeichnun', right_on='Name')
mdf['Sealed (%)'] = 100 - mdf['Green (%)']
avg = mdf[['Sealed (%)', 'Noise (%)', 'PM_10 (%)']].mean(axis=1)
mdf['Multiple Burdens'] = avg


# Spatial Inequality Visualisation
# Berechne Spatial Inequality als Produkt, also wie folgt: SI = SD * MB / 100
mdf['Spatial Inequality'] = mdf['Socioeconomic Disadvantage (%)'] * mdf['Multiple Burdens'] / 100


# Erste Visualisierung: Spatial Inequality als Verhältnis, zunächst mit k=5 Klassen auf einer Choropleth-Karte
k = 5
class_labels = ['sehr gering', 'gering', 'moderat', 'hoch', 'sehr hoch']

quantiles = np.linspace(0, 1, k + 1)
mdf['class'] = pd.qcut(mdf['Spatial Inequality'], q=quantiles, labels=False)
mdf['class_label'] = mdf['class'].map(lambda x: class_labels[x])

base_cmap = plt.cm.Blues
colors = base_cmap(np.linspace(0.2, 1, k))
darker_cmap = mcolors.ListedColormap(colors)
norm = plt.Normalize(vmin=0, vmax=k-1)

fig, ax = plt.subplots(1, 1, figsize=(10, 6))
plot = mdf.plot(column='class', cmap=darker_cmap, linewidth=0.8, ax=ax, edgecolor='0.8', legend=False)

plot.set_title('Spatial Inequality pro Dortmunder Stadtteil')
plot.set_axis_off()

legend_handles = []
for i in range(k):
    color = darker_cmap(norm(i))
    patch = mpatches.Patch(color=color, label=class_labels[i])
    legend_handles.append(patch)

legend = ax.legend(handles=legend_handles, loc='lower left', bbox_to_anchor=(-0.2, 0), title='Relative Spatial-Inequality-\nKlassen:', title_fontproperties={'weight': 'bold'})
legend.get_frame().set_edgecolor('none')

plt.savefig(f'{path_out}SI_classes_5.png', dpi=300, bbox_inches='tight')
plt.show()


# Zweite Visualisierung: Spatial Inequality mit kontinuierlicher Farbskala

fig, ax = plt.subplots(1, 1, figsize=(10, 6))
plot = mdf.plot(column='Spatial Inequality', cmap='Blues', linewidth=0.8, ax=ax, edgecolor='0.8', legend=False)

plot.set_title('Spatial Inequality pro Dortmunder Stadtteil')
plot.set_axis_off()

# Nutze Farben-Range von verhältnismäßig gering bis verhältnismäßig hoch, da die tatsächlichen numerischen Werte jetzt weniger aussagekräftig sind
cbar = plot.get_figure().colorbar(plot.collections[0], ax=ax)
cbar.set_ticks([cbar.vmin, cbar.vmax])
cbar.set_ticklabels(['verhältnismäßig\ngering', 'verhältnismäßig\nhoch']) 

plt.savefig(f'{path_out}SI_continuous.png', dpi=300, bbox_inches='tight')
plt.show()


# Nochmal, mit kontinuierlicher Farbskala und Hervorhebung der 8 Stadtteile mit höchstem Wert
# Sortiere mdf und wähle 8 Stadtteile mit höchstem Wert aus
sorted_mdf = mdf.sort_values(by='Spatial Inequality', ascending=False)
top_8 = sorted_mdf.head(8)

fig, ax = plt.subplots(1, 1, figsize=(10, 6))
plot = mdf.plot(column='Spatial Inequality', cmap='Blues', linewidth=0.8, ax=ax, edgecolor='0.8', legend=False)

top_8.plot(ax=ax, edgecolor='yellow', linewidth=1.2, facecolor='none')

plot.set_title('Spatial Inequality pro Dortmunder Stadtteil')
plot.set_axis_off()

# Nutze Farben-Range von verhältnismäßig gering bis verhältnismäßig hoch, da die tatsächlichen numerischen Werte jetzt weniger aussagekräftig sind
cbar = plot.get_figure().colorbar(plot.collections[0], ax=ax)
cbar.set_ticks([cbar.vmin, cbar.vmax])
cbar.set_ticklabels(['verhältnismäßig\ngering', 'verhältnismäßig\nhoch']) 

plt.savefig(f'{path_out}SI_continuous_8_hotspots_highlighted.png', dpi=300, bbox_inches='tight')
plt.show()


# Da es nun um die Visualiserung des Verhältnisses zweier Variablen geht, bieten sich auch andere Visualisierungsformen an
# Dritte Visualsierung: Spatial Inequality in einem Streudiagramm
plt.figure(figsize=(10, 6))
plt.scatter(mdf['Multiple Burdens'], mdf['Socioeconomic Disadvantage (%)'], color='blue', alpha=0.6)

plt.title('Spatial Inequality pro Dortmunder Stadtteil')
plt.xlabel('Multiple Umweltbelastungen')
plt.ylabel('Sozioökonomischer Nachteil (%)')

# Ändere Beschriftung der x-Achse
plt.xticks([mdf['Multiple Burdens'].min(), mdf['Multiple Burdens'].max()], ['verhältnismäßig\ngering', 'verhältnismäßig\nhoch'])
plt.grid(False)

plt.savefig(f'{path_out}dotplot_social_inequality.png', dpi=300, bbox_inches='tight')

plt.show()


# Noch einmal, mit Labels. Die Farbe wird dafür aufgehellt, damit man die Labels besser lesen kann
plt.figure(figsize=(10, 6))
plt.scatter(mdf['Multiple Burdens'], mdf['Socioeconomic Disadvantage (%)'], color='#5DADE2', alpha=0.6)

plt.title('Spatial Inequality pro Dortmunder Stadtteil')
plt.xlabel('Multiple Umweltbelastungen')
plt.ylabel('Sozioökonomischer Nachteil (%)')

plt.xticks([mdf['Multiple Burdens'].min(), mdf['Multiple Burdens'].max()], ['verhältnismäßig\ngering', 'verhältnismäßig\nhoch'])

texts = []
for i, row in top_8.iterrows():
    texts.append(plt.text(row['Multiple Burdens'], row['Socioeconomic Disadvantage (%)'], row['bezeichnun'], fontsize=9, fontstyle='italic'))

adjust_text(texts)

plt.grid(False)

# Verschiebe rechten Rand des Plots etwas nach rechts, um Platz zu den Labels zu lassen
plt.subplots_adjust(right=0.95)

plt.savefig(f'{path_out}dotplot_social_inequality_labeled.png', dpi=300, bbox_inches='tight')
plt.show()


# Vierte Visualisierung: Spatial Inequality in einem Streudiagramm, mit dynamischer Farbanpassung gemäß des Spatial-Inquality-Werts (Multiple Burdens * Socioeconomic Disadvantage / 100)
plt.figure(figsize=(10, 6))

# Normalisiere Spatial-Inequality-Werte
norm = mcolors.Normalize(vmin=mdf['Spatial Inequality'].min(), vmax=mdf['Spatial Inequality'].max())

# Definiere Colormap
sc = plt.scatter(mdf['Multiple Burdens'], mdf['Socioeconomic Disadvantage (%)'], 
                 c=mdf['Spatial Inequality'], cmap='plasma_r', norm=norm, alpha=0.6) 

plt.title('Spatial Inequality pro Dortmunder Stadtteil')
plt.xlabel('Multiple Umweltbelastungen')
plt.ylabel('Sozioökonomischer Nachteil (%)')

plt.xticks([mdf['Multiple Burdens'].min(), mdf['Multiple Burdens'].max()], ['verhältnismäßig gering', 'verhältnismäßig hoch'])

# Füge Colorbar als Legende hinzu
cbar = plt.colorbar(sc)
cbar.set_label('Spatial Inequality')
cbar.set_ticks([mdf['Spatial Inequality'].min(), mdf['Spatial Inequality'].max()])
cbar.set_ticklabels(['verhältnismäßig\ngering', 'verhältnismäßig\nhoch'])

# Definiere Koordinaten des Colobar-Labels (x,y)
cbar.ax.yaxis.set_label_coords(1.4, 0.5)

texts = []
for i, row in top_8.iterrows():
    texts.append(plt.text(row['Multiple Burdens'], row['Socioeconomic Disadvantage (%)'], row['bezeichnun'], fontsize=9, fontstyle='italic'))

adjust_text(texts)

plt.grid(False)

plt.subplots_adjust(right=0.95)

plt.savefig(f'{path_out}dotplot_social_inequality_colored_dots.png', dpi=300, bbox_inches='tight')
plt.show()


# Fünfte Visualisierung: Spatial Inequality mit kontinuierlicher Farbskala und zusätzlichem Balkendiagramm der 25 Stadtteile mit höchstem Wert
# Wähle Top 25 Stadtteile mit höchstem Spatial-Inequality-Wert aus
top_25 = sorted_mdf.head(25)
# Kehre Reihenfolge um
top_25 = top_25.sort_values(by='Spatial Inequality', ascending=True)

# Erstelle Colormap wie oben
cmap = plt.get_cmap('Blues')
norm = mcolors.Normalize(vmin=mdf['Spatial Inequality'].min(), vmax=mdf['Spatial Inequality'].max())

# Erstelle zwei Subplots und wähle entsprechend höhere figsize
fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(10, 10), gridspec_kw={'height_ratios': [3, 1]})

# Erster Subplot: Choropleth-Karte wie oben (Zweite Visualisierung)
plot = mdf.plot(column='Spatial Inequality', cmap='Blues', linewidth=0.8, ax=ax1, edgecolor='0.8', legend=False)
ax1.set_title('Spatial Inequality pro Dortmunder Stadtteil')
ax1.set_axis_off()
cbar = plot.get_figure().colorbar(plot.collections[0], ax=ax1, orientation='vertical')
cbar.set_ticks([cbar.vmin, cbar.vmax])
cbar.set_ticklabels(['verhältnismäßig\ngering', 'verhältnismäßig\nhoch'])

# Zweiter Suplot: Balkendiagramm
bars = ax2.bar(top_25['bezeichnun'], top_25['Spatial Inequality'], color=cmap(norm(top_25['Spatial Inequality'])))

# Entferne Achsen-Grenzen und -Labels außer die der x-Achse
ax2.spines['top'].set_visible(False)
ax2.spines['right'].set_visible(False)
ax2.spines['left'].set_visible(False)
ax2.spines['bottom'].set_visible(False)
ax2.yaxis.set_visible(False)
ax2.xaxis.set_ticks_position('none') 

# Beschrifte Balkendiagramm
ax2.set_title('Top 25 Stadtteile')
ax2.set_ylabel('Spatial Inequality')

# Beschrifte Balken
ax2.set_xticks(range(len(top_25['bezeichnun'])))
ax2.set_xticklabels(top_25['bezeichnun'], rotation=90)

# Passe Layout an und speichere
plt.tight_layout()
plt.savefig(f'{path_out}SI_continuous_with_barplot_top_25.png', dpi=300, bbox_inches='tight')
plt.show()


# Nochmal, aber mit Stadtteilen mit 10 höchsten und 10 niedrigsten Werten
# Übernehme Sortierung und sortiere nocheinmal in umgekehrter Richtung
sorted_mdf_desc = sorted_mdf
sorted_mdf_asc = mdf.sort_values(by='Spatial Inequality', ascending=True)

# Wähle je 10 Stadtteile mit höchstem und niedrigstem Wert aus
top_10 = sorted_mdf_desc.head(10)
bottom_10 = sorted_mdf_asc.head(10)

# Erstelle leeren DataFrame, der benutzt wird, um im Balkendiagramm eine Lücke zwischen top_10 und bottom_10 zu setzen
gap = pd.DataFrame({'bezeichnun': [''], 'Spatial Inequality': [None]})
# Kombiniere alle drei DataFrames
combined = pd.concat([top_10, gap, bottom_10])
# Passe Ordnung für Visualisierung an
combined = combined.iloc[::-1]

# Jetzt wie oben 
cmap = plt.get_cmap('Blues')
norm = mcolors.Normalize(vmin=mdf['Spatial Inequality'].min(), vmax=mdf['Spatial Inequality'].max())

fig, (ax1, ax2) = plt.subplots(2, 1, figsize=(10, 10), gridspec_kw={'height_ratios': [3, 1]})

plot = mdf.plot(column='Spatial Inequality', cmap='Blues', linewidth=0.8, ax=ax1, edgecolor='0.8', legend=False)
ax1.set_title('Spatial Inequality pro Dortmunder Stadtteil')
ax1.set_axis_off()

cbar = plot.get_figure().colorbar(plot.collections[0], ax=ax1, orientation='vertical')
cbar.set_ticks([cbar.vmin, cbar.vmax])
cbar.set_ticklabels(['verhältnismäßig\ngering', 'verhältnismäßig\nhoch'])

bars = ax2.bar(combined['bezeichnun'], combined['Spatial Inequality'], color=cmap(norm(combined['Spatial Inequality'])))

ax2.spines['top'].set_visible(False)
ax2.spines['right'].set_visible(False)
ax2.spines['left'].set_visible(False)
ax2.spines['bottom'].set_visible(False)
ax2.yaxis.set_visible(False)
ax2.xaxis.set_ticks_position('none') 

ax2.set_title('Unterste 10 und oberste 10 Stadtteile')
ax2.set_ylabel('')

ax2.set_xticks(range(len(combined)))
ax2.set_xticklabels(combined['bezeichnun'], rotation=90)

plt.tight_layout()
plt.savefig(f'{path_out}SI_continuous_with_barplot_top_bottom_10.png', dpi=300, bbox_inches='tight')
plt.show()


