# Dieses Skript erstellt verschiedene Choropleth-Karten zur Visualisierung der auf Flacke et al. basierenden Umweltindikatoren in Dortmund
import pandas as pd
import geopandas as gpd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.patches as mpatches
import matplotlib.colors as mcolors

# Lade CSV und Shapefile
data_path = 'Data/Flacke_Table_S1.csv'
shapefile_path = 'Data/Dortmund/Stat_Unterbezirke/Statist_UnterbezirkePolygon.shp'
path_out = 'Data/Vis/'
ddf = pd.read_csv(data_path)
gdf = gpd.read_file(shapefile_path)

"""
Nicht alle Werte für 'bezeichnun' in gdf entsprechen denen für 'Name' in ddf. Um diejenigen zu identifizieren, die nicht übereinstimmen, nutze:

gdf_unique = set(gdf['bezeichnun'].unique())
ddf_unique = set(ddf['Name'].unique())
gdf_not_in_ddf = gdf_unique - ddf_unique
ddf_not_in_gdf = ddf_unique - gdf_unique

if not gdf_not_in_ddf and not ddf_not_in_gdf:
    print("Unique values in both columns are the same. Proceed with merging.")
else:
    print("Unique values in both columns are not the same. Please verify the data.")
    print("Values in GeoDataFrame not in DataFrame:", gdf_not_in_ddf)
    print("Values in DataFrame not in GeoDataFrame:", ddf_not_in_gdf)
"""
# Sobald bekannt ist, welche Felder nicht übereinstimmen, ändere Felder in gdf entsprechend
# gdf enthält Uppercase, ändere alle Namen zu Titlecase
gdf['bezeichnun'] = gdf['bezeichnun'].apply(lambda x: x.title())

# Sonstige Änderungen, um ggf an ddf anzupassen
gdf.loc[gdf['bezeichnun'] == 'Deutsch-Luxemburger-Straße', 'bezeichnun'] = 'Luxemburger-Straße'
gdf.loc[gdf['bezeichnun'] == 'Siedlung Rotkehlchenweg', 'bezeichnun'] = 'Rotkehlchenweg'
gdf.loc[gdf['bezeichnun'] == 'Msa-Siedlung', 'bezeichnun'] = 'MSA-Siedlung'
gdf.loc[gdf['bezeichnun'] == 'Knappschaftskrankenhaus', 'bezeichnun'] = 'Knappschaftkrankenhaus'

# Merge die beiden Dataframes
mdf = gdf.merge(ddf, left_on='bezeichnun', right_on='Name')

# Berechne 'Sealed' (Versiegelte Flächen) als neue Spalte zum Datensatz hinzu
mdf['Sealed (%)'] = 100 - mdf['Green (%)']

# Berechne Multiple Belastungen (Multiple Burdens) als Mittelwert der Umweltindikatoren
avg = mdf[['Sealed (%)', 'Noise (%)', 'PM_10 (%)']].mean(axis=1)
mdf['Multiple Burdens'] = avg


# Erste Visualisierung: Multiple Umweltbelastungen, angelehnt an Flacke et al. Abb. 4
fig, ax = plt.subplots(1, 1, figsize=(10, 6))
plot = mdf.plot(column='Multiple Burdens', cmap='Greens', linewidth=0.8, ax=ax, edgecolor='0.8', legend=False)

plot.set_title('Multiple Umweltbelastungen pro Dortmunder Stadtteil')
plot.set_axis_off()

# Nutze Farben-Range von verhältnismäßig gering bis verhältnismäßig hoch, da die tatsächlichen numerischen Werte jetzt weniger aussagekräftig sind
cbar = plot.get_figure().colorbar(plot.collections[0], ax=ax)
cbar.set_ticks([cbar.vmin, cbar.vmax])
cbar.set_ticklabels(['verhältnismäßig\ngering', 'verhältnismäßig\nhoch']) 

plt.savefig(f'{path_out}MB_continuous.png', dpi=300, bbox_inches='tight')
plt.show()


# Zweite Visualisierung: wie oben, aber mit Einteilung in k Quintile. Erst mit k=6
# Definiere Klassen und Labels
k = 6
class_labels = ['sehr gering', 'gering', 'moderat', 'hoch', 'sehr hoch', 'extrem hoch']

# Teile in die Klassen ein
quantiles = np.linspace(0, 1, k + 1)
mdf['class'] = pd.qcut(mdf['Multiple Burdens'], q=quantiles, labels=False)
mdf['class_label'] = mdf['class'].map(lambda x: class_labels[x])

# Um zu verhindern, dass die unterste Klasse weiß visualisiert wird und dadurch schlecht sichtbar wird, passe Farbenspektrum etwas an
base_cmap = plt.cm.Greens
# Starte bei 0.2 um sehr helle Farbtöne zu vermeiden
colors = base_cmap(np.linspace(0.2, 1, k))
darker_cmap = mcolors.ListedColormap(colors)
# Normalisiere neues Farbenspektrum
norm = plt.Normalize(vmin=0, vmax=k-1)


fig, ax = plt.subplots(1, 1, figsize=(10, 6))
plot = mdf.plot(column='class', cmap=darker_cmap, linewidth=0.8, ax=ax, edgecolor='0.8', legend=False)

plot.set_title('Multiple Umweltbelastungen pro Dortmunder Stadtteil')
plot.set_axis_off()

# Passe Legende an
legend_handles = []
for i in range(k):
    color = darker_cmap(norm(i))
    patch = mpatches.Patch(color=color, label=class_labels[i])
    legend_handles.append(patch)

legend = ax.legend(handles=legend_handles, loc='lower left', bbox_to_anchor=(-0.2, 0), title='Relative Belastungsklassen:', title_fontproperties={'weight': 'bold'})
legend.get_frame().set_edgecolor('none')

plt.savefig(f'{path_out}MB_classes_6.png', dpi=300, bbox_inches='tight')
plt.show()

"""
Die Karte entspricht nicht ganz der von Flacke et al. Leider geben sie keine Aukunft darüber, wie sie ihren Parameter Multiple Burdens berechnen. Wenn ich statt nur einen beide Luftqualitäts-Indikatoren dazunehme, ändert sich meine Visualisierung im Übrigen nicht
"""


# Und nochmal, mit k=4
k = 4
class_labels = ['gering', 'moderat', 'hoch', 'sehr hoch']

quantiles = np.linspace(0, 1, k + 1)
mdf['class'] = pd.qcut(mdf['Multiple Burdens'], q=quantiles, labels=False)
mdf['class_label'] = mdf['class'].map(lambda x: class_labels[x])

base_cmap = plt.cm.Greens
colors = base_cmap(np.linspace(0.2, 1, k))
darker_cmap = mcolors.ListedColormap(colors)
norm = plt.Normalize(vmin=0, vmax=k-1)

fig, ax = plt.subplots(1, 1, figsize=(10, 6))
plot = mdf.plot(column='class', cmap=darker_cmap, linewidth=0.8, ax=ax, edgecolor='0.8', legend=False)

plot.set_title('Multiple Umweltbelastungen pro Dortmunder Stadtteil')
plot.set_axis_off()

legend_handles = []
for i in range(k):
    color = darker_cmap(norm(i))
    patch = mpatches.Patch(color=color, label=class_labels[i])
    legend_handles.append(patch)

legend = ax.legend(handles=legend_handles, loc='lower left', bbox_to_anchor=(-0.2, 0), title='Relative Belastungsklassen:', title_fontproperties={'weight': 'bold'})
legend.get_frame().set_edgecolor('none')

plt.savefig(f'{path_out}MB_classes_4.png', dpi=300, bbox_inches='tight')
plt.show()



