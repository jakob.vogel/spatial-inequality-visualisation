# Dieses Skript erstellt verschiedene Choropleth-Karten zur Visualisierung des auf Flacke et al. basierenden Sozioökonomischen Nachteils in Dortmund
import pandas as pd
import geopandas as gpd
import matplotlib.pyplot as plt
import numpy as np
import rasterio
from scipy.interpolate import griddata
from rasterio.features import geometry_mask
from adjustText import adjust_text
from shapely.ops import unary_union

# Lade CSV und Shapefile
data_path = 'Data/Flacke_Table_S1.csv'
shapefile_path = 'Data/Dortmund/Stat_Unterbezirke/Statist_UnterbezirkePolygon.shp'
path_out = 'Data/Vis/'
ddf = pd.read_csv(data_path)
gdf = gpd.read_file(shapefile_path)

"""
Nicht alle Werte für 'bezeichnun' in gdf entsprechen denen für 'Name' in ddf. Um diejenigen zu identifizieren, die nicht übereinstimmen, nutze:

gdf_unique = set(gdf['bezeichnun'].unique())
ddf_unique = set(ddf['Name'].unique())
gdf_not_in_ddf = gdf_unique - ddf_unique
ddf_not_in_gdf = ddf_unique - gdf_unique

if not gdf_not_in_ddf and not ddf_not_in_gdf:
    print("Unique values in both columns are the same. Proceed with merging.")
else:
    print("Unique values in both columns are not the same. Please verify the data.")
    print("Values in GeoDataFrame not in DataFrame:", gdf_not_in_ddf)
    print("Values in DataFrame not in GeoDataFrame:", ddf_not_in_gdf)
"""
# Sobald bekannt ist, welche Felder nicht übereinstimmen, ändere Felder in gdf entsprechend
# gdf enthält Uppercase, ändere alle Namen zu Titlecase
gdf['bezeichnun'] = gdf['bezeichnun'].apply(lambda x: x.title())

# Sonstige Änderungen, um ggf an ddf anzupassen
gdf.loc[gdf['bezeichnun'] == 'Deutsch-Luxemburger-Straße', 'bezeichnun'] = 'Luxemburger-Straße'
gdf.loc[gdf['bezeichnun'] == 'Siedlung Rotkehlchenweg', 'bezeichnun'] = 'Rotkehlchenweg'
gdf.loc[gdf['bezeichnun'] == 'Msa-Siedlung', 'bezeichnun'] = 'MSA-Siedlung'
gdf.loc[gdf['bezeichnun'] == 'Knappschaftskrankenhaus', 'bezeichnun'] = 'Knappschaftkrankenhaus'

# Merge die beiden Dataframes
mdf = gdf.merge(ddf, left_on='bezeichnun', right_on='Name')


# Erste Visualisierung: Soziökonimischer Nachteil, entspricht Flacke et al. Abb. 2. Anders als die Autor*innen benutze ich eine kontinuierliche Variable. Urbane Stadtteile hebe ich nicht hervor, weil weder der veröffenltichte Datensatz noch das Paper Informationen hierzu liefern
fig, ax = plt.subplots(1, 1, figsize=(10, 6))
plot = mdf.plot(column='Socioeconomic Disadvantage (%)', cmap='OrRd', linewidth=0.8, ax=ax, edgecolor='0.8', legend=True)
plot.set_title('Soziökonomischer Nachteil in % pro Dortmunder Stadtteil')
plot.set_axis_off()

plt.savefig(f'{path_out}SN_normal.png', dpi=300, bbox_inches='tight')
plt.show()


# Zweitens: Soziökonimischer Nachteil, entspricht Flacke et al. Abb. 2, mit Clarenberg hervorgehoben
fig, ax = plt.subplots(1, 1, figsize=(10, 6))
plot = mdf.plot(column='Socioeconomic Disadvantage (%)', cmap='OrRd', linewidth=0.8, ax=ax, edgecolor='0.8', legend=True)

clarenberg = mdf[mdf['bezeichnun'] == 'Clarenberg']
clarenberg.plot(ax=ax, edgecolor='yellow', facecolor='none', linewidth=1.2)

plot.set_title('Soziökonomischer Nachteil in % pro Dortmunder Stadtteil')
plot.set_axis_off()

plt.savefig(f'{path_out}SN_highlight_Clarenberg.png', dpi=300, bbox_inches='tight')
plt.show()


# Drittens: Soziökonimischer Nachteil, entspricht Flacke et al. Abb. 2, mit Labels
fig, ax = plt.subplots(1, 1, figsize=(10, 6))
plot = mdf.plot(column='Socioeconomic Disadvantage (%)', cmap='OrRd', linewidth=0.8, ax=ax, edgecolor='0.8', legend=True)

plot.set_title('Soziökonomischer Nachteil in % pro Dortmunder Stadtteil')
plot.set_axis_off()

# Benutze Uppercase-Buchstaben des Namens als Label
labels = []
for idx, row in mdf.iterrows():
    label = ''.join(filter(str.isupper, row['Name'])) 
    labels.append(plt.text(row['geometry'].centroid.x, row['geometry'].centroid.y, label, fontsize=8, ha='center'))

# Benutze adjust_text um Overlap von Labels zu vermeiden
adjust_text(labels, arrowprops=dict(arrowstyle="->", color='r', lw=0.5))

plt.savefig(f'{path_out}SN_labels_short.png', dpi=300, bbox_inches='tight')
plt.show()

"""
Um Overlap zu vermeiden, werden die Labels stellenweise verschoben. Die Zuordnung fällt dann bei manchen Shapes nicht mehr ganz einfach
"""


# Noch einmal, mit vollem Namen
fig, ax = plt.subplots(1, 1, figsize=(10, 6))
plot = mdf.plot(column='Socioeconomic Disadvantage (%)', cmap='OrRd', linewidth=0.8, ax=ax, edgecolor='0.8', legend=True)

plot.set_title('Soziökonomischer Nachteil in % pro Dortmunder Stadtteil')
plot.set_axis_off()

labels = []
for idx, row in mdf.iterrows():
    label = row['Name']
    labels.append(plt.text(row['geometry'].centroid.x, row['geometry'].centroid.y, label, fontsize=8, ha='center'))

adjust_text(labels, arrowprops=dict(arrowstyle="->", color='r', lw=0.5))

plt.savefig(f'{path_out}SN_labels_full.png', dpi=300, bbox_inches='tight')
plt.show()


# Noch einmal, nur mit Labels an Statdtteilen mit Sozioök. Nachteil über 25
disadvantaged_threshold = 25

fig, ax = plt.subplots(1, 1, figsize=(10, 6))
plot = mdf.plot(column='Socioeconomic Disadvantage (%)', cmap='OrRd', linewidth=0.8, ax=ax, edgecolor='0.8', legend=True)

plot.set_title('Soziökonomischer Nachteil in % pro Dortmunder Stadtteil')
plot.set_axis_off()

labels = []
for idx, row in mdf.iterrows():
    if row['Socioeconomic Disadvantage (%)'] >= disadvantaged_threshold:
        label = row['Name']
        labels.append(plt.text(row['geometry'].centroid.x, row['geometry'].centroid.y, label, fontsize=8, ha='center'))

adjust_text(labels, arrowprops=dict(arrowstyle="->", color='r', lw=0.5))

plt.savefig(f'{path_out}SN_labels_long_if_min_25.png', dpi=300, bbox_inches='tight')
plt.show()


# Viertens: Hervorheben von Clustern Sozioök. Nachteil, einmal mit Treshold 20
disadvantaged_treshold = 20

# Filtere Datensatz nach Stadtteilen deren Sozioök. Nachteil über dem Treshold liegen
disadvantaged_neighborhoods = mdf[mdf['Socioeconomic Disadvantage (%)'] > disadvantaged_treshold]
# Idenitifiziere Cluster, grupiere benachbarate Polygone zu Multi-Polygonen mit unary_union
clusters = disadvantaged_neighborhoods.geometry.unary_union

# Plotte wie oben
fig, ax = plt.subplots(1, 1, figsize=(10, 6))
plot = mdf.plot(column='Socioeconomic Disadvantage (%)', cmap='OrRd', linewidth=0.8, ax=ax, edgecolor='0.8', legend=True)
plot.set_title('Soziökonomischer Nachteil in % pro Dortmunder Stadtteil')
plot.set_axis_off()

if isinstance(clusters, (gpd.geoseries.GeoSeries, gpd.geodataframe.GeoDataFrame)):
    # Behandle Fälle in denen unary_union nach wie vor eine GeoSeries oder einen GeoDataFrame zurückgibt
    gpd.GeoSeries(clusters).plot(ax=ax, facecolor='none', edgecolor='blue', linewidth=1.2)
else:
    # Behandle Fälle in denen unary_union ein einheitliches (Multi-)Polygon zurückgibt
    gpd.GeoSeries([clusters]).plot(ax=ax, facecolor='none', edgecolor='blue', linewidth=1.2)

plt.savefig(f'{path_out}SN_clustered_treshold_20.png', dpi=300, bbox_inches='tight')
plt.show()


# Noch einmal mit Treshold 25
disadvantaged_treshold = 25
disadvantaged_neighborhoods = mdf[mdf['Socioeconomic Disadvantage (%)'] > disadvantaged_treshold]
clusters = disadvantaged_neighborhoods.geometry.unary_union
fig, ax = plt.subplots(1, 1, figsize=(10, 6))
plot = mdf.plot(column='Socioeconomic Disadvantage (%)', cmap='OrRd', linewidth=0.8, ax=ax, edgecolor='0.8', legend=True)
plot.set_title('Soziökonomischer Nachteil in % pro Dortmunder Stadtteil')
plot.set_axis_off()
if isinstance(clusters, (gpd.geoseries.GeoSeries, gpd.geodataframe.GeoDataFrame)):
    gpd.GeoSeries(clusters).plot(ax=ax, facecolor='none', edgecolor='blue', linewidth=1.2)
else:
    gpd.GeoSeries([clusters]).plot(ax=ax, facecolor='none', edgecolor='blue', linewidth=1.2)
plt.savefig(f'{path_out}SN_clustered_treshold_25.png', dpi=300, bbox_inches='tight')
plt.show()


# Fünftes: Soziökonimischer Nachteil, Blautöne
fig, ax = plt.subplots(1, 1, figsize=(10, 6))
plot = mdf.plot(column='Socioeconomic Disadvantage (%)', cmap='Blues', linewidth=0.8, ax=ax, edgecolor='0.8', legend=True)
plot.set_title('Soziökonomischer Nachteil in % pro Dortmunder Stadtteil')
plot.set_axis_off() 

plt.savefig(f'{path_out}SN_blue.png', dpi=300, bbox_inches='tight')
plt.show()


# Noch einmal invertiert (dunkel = niedrige Werte, hell = hohe Werte)
fig, ax = plt.subplots(1, 1, figsize=(10, 6))
plot = mdf.plot(column='Socioeconomic Disadvantage (%)', cmap='Blues_r', linewidth=0.8, ax=ax, edgecolor='0.8', legend=True)
plot.set_title('Soziökonomischer Nachteil in % pro Dortmunder Stadtteil')
plot.set_axis_off()  

plt.savefig(f'{path_out}SN_blue_inverted.png', dpi=300, bbox_inches='tight')
plt.show()


# Sechstens: Smoothing der ersten Visualisierung
# Benutze für das Smoothing rasterio und griddata. Hierbei werden die Datenwerte auf ein hochauflösendes Rastergitter interpoliert

# Extrahiere Koordinaten und Datenwerte
points = np.array([mdf.geometry.centroid.x, mdf.geometry.centroid.y]).T
values = mdf['Socioeconomic Disadvantage (%)'].values

# Initiiere hochauflösendes Raster
grid_resolution = 500 
grid_x, grid_y = np.mgrid[mdf.total_bounds[0]:mdf.total_bounds[2]:grid_resolution*1j, 
                          mdf.total_bounds[1]:mdf.total_bounds[3]:grid_resolution*1j]

# Interpoliere Werte auf das Raster
grid_z = griddata(points, values, (grid_x, grid_y), method='cubic')

# Initiiere Maske, um eingefärbten Bereich auf Polygone einzuschränken
transform = rasterio.transform.from_bounds(*mdf.total_bounds, grid_resolution, grid_resolution)
shapes = [(geom, 1) for geom in mdf.geometry]
mask = geometry_mask(shapes, transform=transform, invert=True, out_shape=grid_z.shape)

# Wende Maske auf das Raster an
grid_z[~mask] = np.nan

fig, ax = plt.subplots(1, 1, figsize=(10, 6))

# Plotte das Raster
im = ax.imshow(grid_z.T, extent=(mdf.total_bounds[0], mdf.total_bounds[2], 
                                 mdf.total_bounds[1], mdf.total_bounds[3]), origin='lower',
              cmap='OrRd', alpha=0.8)

# Plotte die Ränder
mdf.boundary.plot(ax=ax, linewidth=1, edgecolor='0.8')

cbar = plt.colorbar(im, ax=ax)

ax.set_title('Soziökonomischer Nachteil in % pro Dortmunder Stadtteil')
ax.set_axis_off()

plt.savefig(f'{path_out}SN_smoothed.png', dpi=300, bbox_inches='tight')
plt.show()


