# Dieses Skript rekonstruiert die Regressionsanalyse von Helbig und Salomo für den Indikator 'Anteil Industrie- und Gewerbeflächen' für das Fallbeispiel Dortmund (Helbig und Salomo, Tabelle A 4)
import pandas as pd
import statsmodels.api as sm

# Lade Daten
path_data = 'Data/Helbig_Salomo_Daten.xlsx'
path_out = 'Data/An/'
df = pd.read_excel(path_data)

df_do = df.loc[df['Stadt'] == 'Dortmund']

# Im Anhang des Papers wird die Distanz in km statt in m angegeben. Ändere DataFrame entsprechend
df_do = df_do.copy()
df_do['Distanz Stadtzentroid (km)'] = df_do['Distanz Stadtzentroid (m)'] / 1000

# Definiere Quintile und y-Variable
quintiles = ['SGB-II-Quote der unter 15-Jährigen (%): 1. Quintil',
             'SGB-II-Quote der unter 15-Jährigen (%): 2. Quintil',
             'SGB-II-Quote der unter 15-Jährigen (%): 3. Quintil',
             'SGB-II-Quote der unter 15-Jährigen (%): 4. Quintil',
             'SGB-II-Quote der unter 15-Jährigen (%): 5. Quintil']
y_variable = 'Anteil Industriefläche an Gesamtfläche (%)'

# Entferne erstes Quintil, welches als Referenz, d.h. als Kontante, verwendet wird
four_quintiles = quintiles[1:]
# Initiiere zweite Liste mit zusätzlicher Distanzvariabel für alternative Regression
four_quintiles_dist = [x for x in four_quintiles]
four_quintiles_dist.append('Distanz Stadtzentroid (km)')


# Erste Regression: Anteil Industrie- und Gewerbeflächen ohne Distanzvariable
# Definiere Design-Matrix mit Quintilen 2-5 und der y-Variablen
x_variables = four_quintiles
y_variable = 'Anteil Industriefläche an Gesamtfläche (%)'

X = df_do[x_variables]
y = df_do[y_variable]

# Füge Konstante (intercept) zur Matrix hinzu, die das erste Quintil repräsentiert
X = sm.add_constant(X)
# Fitting
model = sm.OLS(y,X)
result = model.fit()

# Gib normale Regressionkoeffizienten aus
print('Normal regression coefficients:')
print(result.params)

# Speichere normale Regressionskoeffizienten
params_df = pd.DataFrame(result.params, columns=['Coefficient'])
params_df.to_csv(f'{path_out}Helbig_und_Salomo_normale_regressionskoeffizienten_ohne_Distanz.csv', index_label='Variable')

# Berechen standardisierte beta-Koffizienten: b = (std(x)/std(y))*coef
# Berechne coef-Faktor
std_y = df_do[y_variable].std() 
std_x = [df_do[x].std() for x in x_variables]
std_xy_divided = [x/std_y for x in std_x]
# Initialisiere beta-Liste
beta = []
# Iteriere über Koeffizienten, um beta zu berechnen, überspringe result.params[0], da das die Konstante ist 
for i in range(0,len(std_xy_divided)):
    coef = result.params[i+1]
    factor = std_xy_divided[i]
    beta.append(factor*coef)
# Gib beta-Koeffizienten aus
print('\nStandardized beta coefficients:')
print('const:\t-')
for j in range(0,len(beta)):
    print(f'SGB-II-Quote der unter 15-Jährigen (%): {j+2}. Quintil:\t{beta[j]}')

# Speichere beta-Koeffizienten    
beta_df = pd.DataFrame(beta, index=x_variables, columns=['Standardized Beta'])
beta_df.to_csv(f'{path_out}Helbig_und_Salomo_beta_Koeffizienten_ohne_Distanz.csv', index_label='Variable')

    
# Gib Zusammenfassung des Models aus
print('\n', result.summary())


# Zweite Regression: Anteil Industrie- und Gewerbeflächen mit Distanzvariable

# Redefiniere Variablen
x_variables = four_quintiles_dist
y_variable = 'Anteil Industriefläche an Gesamtfläche (%)'
X = df_do[x_variables]
y = df_do[y_variable]

# Jetzt wie oben
X = sm.add_constant(X)
model = sm.OLS(y,X)
result = model.fit()
print('Normal regression coefficients:')
print(result.params)
params_df = pd.DataFrame(result.params, columns=['Coefficient'])
params_df.to_csv(f'{path_out}Helbig_und_Salomo_normale_regressionskoeffizienten_mit_Distanz.csv', index_label='Variable')

std_y = df_do[y_variable].std() 
std_x = [df_do[x].std() for x in x_variables]
std_xy_divided = [x/std_y for x in std_x]
beta = []
for i in range(0,len(std_xy_divided)):
    coef = result.params[i+1]
    factor = std_xy_divided[i]
    beta.append(factor*coef)
print('\nStandardized beta coefficients:')
print('const:\t-')
for j in range(0,len(beta)):
    if j == 4:
        print(f'Distanz Stadtzentroid (km): \t{beta[j]}')
    else:
        print(f'SGB-II-Quote der unter 15-Jährigen (%): {j+2}. Quintil:\t{beta[j]}')
beta_df = pd.DataFrame(beta, index=x_variables, columns=['Standardized Beta'])
beta_df.to_csv(f'{path_out}Helbig_und_Salomo_beta_Koeffizienten_mit_Distanz.csv', index_label='Variable')
print('\n', result.summary())

