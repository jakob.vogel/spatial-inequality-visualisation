# Dieses Skript berechnet die Spearman-Rankkorrelation zwischen den verschiedenen räumlichen Indikatoren, die Helbig und Salomo benutzen
import pandas as pd
from scipy.stats import spearmanr

# Lade Daten
data_path = 'Data/Helbig_Salomo_Daten.xlsx'
path_out = 'Data/An/'
df = pd.read_excel(data_path)

# Initiiere zweiten DataFrame, wähle nur Zeilen, die Dortmunder Stadtteile repräsentieren
df_do = df[df['Stadt'] == 'Dortmund']

# Definiere relevante Spalten
columns_of_interest = ['Distanz Stadtzentroid (m)',
                       'Anteil Industriefläche an Gesamtfläche (%)',
                       'Anteil Wohnfläche Lärmbelastung Nacht =>50 dB (m2) an Gesamtwohnfläche',
                       'Anteil Wohnfläche Mehrfachlärmbelastung Nacht =>50 dB (m2) an Gesamtwohnfläche',
                       'Quadratmeter Sport-, Freizeit-, Erholungsfläche pro Kind unter 15 Jahren',
                       'Quadratmeter Sport-, Freizeit-, Erholungs-, Freiraumfläche pro Kind unter 15 Jahren',
                       'Spielplatzfläche (m2)',
                       'Straßennetz verkehrsberuhigte Zonen (Tempo < 30 km/h) (%)',
                       'Anteil Straßennetz Spielstraßen an Gesamtstraßennetz (%)',
                       'Distanz Bibliothek (m)',
                       'Distanz Musikschule (m)',
                       'Distanz Kindertheater (m)',
                       'Distanz Theater, Oper, Konzerthaus (m)',
                       'Distanz Bad',
                       'Distanz Gymnasiale Oberstufe (m)',
                       'Distanz Ganztagsschule (m)',
                       'Distanz Kinderarzt (m)',
                       'Distanz Kinder- und Jugendtherapeut (m)']
                       
                       
# Benutze Pandas' Spearman-Methode um Korrelationen zwischen räumlichen Indikatoren festzustellen, einmal für Gesamtdatensatz...
spearman_corr = df[columns_of_interest].corr(method='spearman')

# Berechne P-Werte
p_values = df[columns_of_interest].apply(lambda x: df[columns_of_interest].apply(lambda y: spearmanr(x, y)[1]))

# Runde Werte auf 3 Nachkommastellen
spearman_corr_rounded = spearman_corr.round(3)
p_values_rounded = p_values.round(3)

# Speichere Tabellen und gib sie auf der Konsole aus
spearman_corr_rounded.to_csv(f'{path_out}helbig_spearman_rank_coefficients_ges.csv')
p_values_rounded.to_csv(f'{path_out}helbig_p_values_ges.csv')

print('\nSpearman-Rangkorrelation gesamt:')
print(spearman_corr_rounded)
print('--------------------------------')

print('\nP-Werte gesamt:')
print(p_values_rounded)
print('--------------------------------')

# Gib Wahrheitsmatrix aus, die abbildet, welche der P-Werte unter 0.05 liegen (also derselbe Test, den Flacke et al. durchführen)
is_below_005 = p_values_rounded < 0.05
print('\nP-Werte unter 0.05 gesamt:')
print(is_below_005)


# ... und nochmal nur für Dortmund-Datensatz
spearman_corr = df_do[columns_of_interest].corr(method='spearman')

# Berechne P-Werte
p_values = df_do[columns_of_interest].apply(lambda x: df_do[columns_of_interest].apply(lambda y: spearmanr(x, y)[1]))

spearman_corr_rounded = spearman_corr.round(3)
p_values_rounded = p_values.round(3)

spearman_corr_rounded.to_csv(f'{path_out}helbig_spearman_rank_coefficients_dortmund.csv')
p_values_rounded.to_csv(f'{path_out}helbig_p_values_dortmund.csv')

print('\nSpearman-Rangkorrelation Dortmund:')
print(spearman_corr_rounded)
print('--------------------------------')

print('\nP-Werte Dortmund:')
print(p_values_rounded)
print('--------------------------------')

is_below_005 = p_values_rounded < 0.05
print('\nP-Werte unter 0.05 Dortmund:')
print(is_below_005)


# Jetzt soll noch überprüft werden, ob Distanz Stadtzentroid (m) mit der SGB-II-Quote korreliert, auch einmal für den gesamten Datensatz und einmal nur für Dortmund
columns_of_interest = ['Distanz Stadtzentroid (m)',
                       'SGB-II-Quote der unter 15-Jährigen (%): 1. Quintil',
                       'SGB-II-Quote der unter 15-Jährigen (%): 2. Quintil',
                       'SGB-II-Quote der unter 15-Jährigen (%): 3. Quintil',
                       'SGB-II-Quote der unter 15-Jährigen (%): 4. Quintil',
                       'SGB-II-Quote der unter 15-Jährigen (%): 5. Quintil']

spearman_corr = df[columns_of_interest].corr(method='spearman')
p_values = df[columns_of_interest].apply(lambda x: df[columns_of_interest].apply(lambda y: spearmanr(x, y)[1]))
spearman_corr_rounded = spearman_corr.round(3)
p_values_rounded = p_values.round(3)
spearman_corr_rounded.to_csv(f'{path_out}helbig_spearman_rank_coefficients_zentrum_ges.csv')
p_values_rounded.to_csv(f'{path_out}helbig_p_values_zentrum_ges.csv')
print('\nSpearman-Rangkorrelation gesamt:')
print(spearman_corr_rounded)
print('--------------------------------')
print('\nP-Werte gesamt:')
print(p_values_rounded)
print('--------------------------------')
is_below_005 = p_values_rounded < 0.05
print('\nP-Werte unter 0.05 gesamt:')
print(is_below_005)

spearman_corr = df_do[columns_of_interest].corr(method='spearman')
p_values = df_do[columns_of_interest].apply(lambda x: df_do[columns_of_interest].apply(lambda y: spearmanr(x, y)[1]))
spearman_corr_rounded = spearman_corr.round(3)
p_values_rounded = p_values.round(3)
spearman_corr_rounded.to_csv(f'{path_out}helbig_spearman_rank_coefficients_zentrum_dortmund.csv')
p_values_rounded.to_csv(f'{path_out}helbig_p_values_zentrum_dortmund.csv')
print('\nSpearman-Rangkorrelation Dortmund:')
print(spearman_corr_rounded)
print('--------------------------------')
print('\nP-Werte Dortmund:')
print(p_values_rounded)
print('--------------------------------')
is_below_005 = p_values_rounded < 0.05
print('\nP-Werte unter 0.05 Dortmund:')
print(is_below_005)


# Jetzt soll noch überprüft werden, ob Distanz Stadtzentroid (m) mit der SGB-II-Quote korreliert, auch einmal für den gesamten Datensatz und einmal nur für Dortmund
# SGB-II-Quote ist in Qunintilen mit separaten Spalten kodiert. Spalten müssen erst gemerged werden
columns_to_merge = ['SGB-II-Quote der unter 15-Jährigen (%): 1. Quintil',
                       'SGB-II-Quote der unter 15-Jährigen (%): 2. Quintil',
                       'SGB-II-Quote der unter 15-Jährigen (%): 3. Quintil',
                       'SGB-II-Quote der unter 15-Jährigen (%): 4. Quintil',
                       'SGB-II-Quote der unter 15-Jährigen (%): 5. Quintil']

# Definiere Funktion, die Zugehörigkeit zu Quintil ermittelt und zurückgibt
def sgb(row):
    for i, column in enumerate(columns_to_merge):
        if row[column] == 1:
            return i + 1 
    return None  # Falls keine Zugehörigkeit ermittelt wird

# Update die beiden DataFrames
df['SGB-II Quintil'] = df.apply(sgb, axis=1)
df_do = df.copy()
df_do['SGB-II Quintil'] = df_do.apply(sgb, axis=1)

# Berechne Spearman-Rangkorrelation zwischen Entfernung zum Stadtzentrum und SGB-II-Quote, wiederum einmal für den gesamten Datensatz und einmal für Dortmund
columns_of_interest = ['Distanz Stadtzentroid (m)', 'SGB-II Quintil']
spearman_corr = df[columns_of_interest].corr(method='spearman')
p_values = df[columns_of_interest].apply(lambda x: df[columns_of_interest].apply(lambda y: spearmanr(x, y)[1]))
spearman_corr_rounded = spearman_corr.round(3)
p_values_rounded = p_values.round(3)
spearman_corr_rounded.to_csv(f'{path_out}helbig_spearman_rank_coefficients_zentrum_ges.csv')
p_values_rounded.to_csv(f'{path_out}helbig_p_values_zentrum_ges.csv')
print('Spearman-Rangkorrelation gesamt:')
print(spearman_corr_rounded)
print('--------------------------------')
print('\nP-Werte gesamt:')
print(p_values_rounded)
print('--------------------------------')
is_below_005 = p_values_rounded < 0.05
print('\nP-Werte unter 0.05 gesamt:')
print(is_below_005)

spearman_corr = df_do[columns_of_interest].corr(method='spearman')
p_values = df_do[columns_of_interest].apply(lambda x: df_do[columns_of_interest].apply(lambda y: spearmanr(x, y)[1]))
spearman_corr_rounded = spearman_corr.round(3)
p_values_rounded = p_values.round(3)
spearman_corr_rounded.to_csv(f'{path_out}helbig_spearman_rank_coefficients_zentrum_dortmund.csv')
p_values_rounded.to_csv(f'{path_out}helbig_p_values_zentrum_dortmund.csv')
print('\nSpearman-Rangkorrelation Dortmund:')
print(spearman_corr_rounded)
print('--------------------------------')
print('\nP-Werte Dortmund:')
print(p_values_rounded)
print('--------------------------------')
is_below_005 = p_values_rounded < 0.05
print('\nP-Werte unter 0.05 Dortmund:')
print(is_below_005)

"""
Für Dortmund lässt sich keine statistisch signifikante Beziehung zwischen der Entfernung zum Stadtzentrum und der SGB-II-Quote festellen
"""